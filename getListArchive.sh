#!/bin/bash

if test "$1" == ""
then
    echo "Syntax error: $0 <listname>"
    exit 1
fi

web="https://lists.gforge.inria.fr/pipermail"

begin=0
end=999999

# let's suppose the list was created not before january 2000
yearMonth="2000-January"
endOfTime=$(LC_ALL=C date "+%Y-%B" --date='next Month')

listname="$1"
webpage="${web}/${listname}"
output=archives/${listname}


wget -q -O /tmp/$$.html $webpage/index.html
if test ! -s /tmp/$$.html
then
    echo "Error. List $listname invalid (URL $webpage/index.html not found)"
    exit 1
fi

function getNextMonth()
{
    year=$(echo $1 | cut -f1 -d'-')
    year1=$(echo $year + 1 | bc -l)
    month=$(echo $1 | cut -f2 -d'-')
    if test "$month" == "January" ; then echo "$year-February" ; fi
    if test "$month" == "February" ; then echo "$year-March" ; fi
    if test "$month" == "March" ; then echo "$year-April" ; fi
    if test "$month" == "April" ; then echo "$year-May" ; fi
    if test "$month" == "May" ; then echo "$year-June" ; fi
    if test "$month" == "June" ; then echo "$year-July" ; fi
    if test "$month" == "July" ; then echo "$year-August" ; fi
    if test "$month" == "August" ; then echo "$year-September" ; fi
    if test "$month" == "September" ; then echo "$year-October" ; fi
    if test "$month" == "October" ; then echo "$year-November" ; fi
    if test "$month" == "November" ; then echo "$year-December" ; fi
    if test "$month" == "December" ; then echo "$year1-January" ; fi
}

function getMonthNumber()
{
    case "$1" in
	"January" ) echo "01" ;;
	"February" ) echo "02" ;;
	"March" ) echo "03" ;;
	"April" ) echo "04" ;;
	"May" ) echo "05";;
	"June" ) echo "06";;
	"July" ) echo "07";;
	"August" ) echo "08" ;;
	"September" ) echo "09" ;;
	"October" ) echo "10" ;;
	"November" ) echo "11" ;;
	"December" ) echo "12" ;;
    esac
}

for n in $(seq -w $begin $end)
do
    #echo "Testing message $n"
    while true
    do
	found=0
	echo "checking $webpage/$yearMonth/$n.html"
	wget -q -P $output $webpage/$yearMonth/$n.html
	if test ! -f $output/$n.html
	then
	    yearMonth=$(getNextMonth $yearMonth)
	    if test "$yearMonth" == "$endOfTime"
	    then
		break
	    fi
	else
	    found=1
	    xyear=$(echo $yearMonth | awk -F'-' '{print $1}')
	    xmonth=$(getMonthNumber $(echo $yearMonth | awk -F'-' '{print $2}'))
	    mkdir -p $output/${xyear}-${xmonth}
	    mv $output/$n.html $output/${xyear}-${xmonth}/msg$n.html
	fi
	if test "$found" == "1"
	then
	    break
	fi
    done

    if test "$yearMonth" == "$endOfTime"
    then
	break
    fi
done
